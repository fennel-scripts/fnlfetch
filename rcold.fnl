#!/usr/bin/env fennel
(local pallet [
		"%{blackbg}"
		"%{redbg}"
		"%{greenbg}"
		"%{yellowbg}"
		"%{bluebg}"
		"%{magentabg}"
		"%{cyanbg}"
		"%{whitebg}"
		"%{reset}"
	]
)
;;"%{}"	"##"
;;"%{}"	"##"
;;"%{}"	"##"
;;"%{}"	"##"
;;"%{}"	"##"



(local data {
:default
	[
	 {:key "       %{blue}erik%{reset}@" :value "%{blue}debianLaptop"   }
	 {:key "      [========" :value "=========]  "}
	 {:key "%{blue}OS               %{reset}" :value ":debian"      }
	 {:key "%{blue}Home Directory   %{reset}" :value ":/home/erik"  }
	 {:key "%{blue}shell            %{reset}" :value ":/bin/zsh"    }
	 {:key "%{blue}Window Manager   %{reset}" :value ":Awesome"     }
	 {:key "%{blue}Editor           %{reset}" :value ":Emacs"       }
	 {:key "                 " :value (table.concat pallet "   ")}
	 {:key "                 " :value " "}
	 {:key "                 " :value " "}
	 {:key "                 " :value " "}
	 {:key "                 " :value " "}
	 {:key "                 " :value " "}
	]
})

;;      {:key "%{blue}username         %{reset}" :value ":erik"            }


(local rc {
:logo "tardis"
:logo "startrekColour"
;;:logo "startrek"
:data (. data :default)

})
rc
