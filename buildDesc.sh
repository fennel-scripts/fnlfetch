#!/usr/bin/env bash
alias fennel="/usr/bin/env fennel"
#transpile:
#	$(fennel) -c main.fnl main.lua

function config
{
	cp logos.fnl.default $XDG_CONFIG_DIR/fnlFetch/logos.fnl
	cp rc.fnl.default    $XDG_CONFIG_DIR/fnlFetch/rc.fnl
}

function build
{
	cp main.fnl fnlFetch
}



function inst
{
	build
	sudo install -Dm755 fnlFetch /usr/bin/fnlFetch
}



function clean
{
	inst
	rm -f fnlFetch
}

$@
