#!/usr/bin/env fennel
(local fennel (require :fennel))
(table.insert (or package.loaders package.searchers) fennel.searcher)
(local confpath (os.getenv :XDG_CONFIG_DIR))
(set package.path (.. package.path ";" confpath "/fnlFetch/?.fnl"))
(set fennel.path (.. fennel.path ";" confpath :/fnlFetch/?.fnl))
(table.insert package.path (.. confpath "fnlfetch/?.fnl"))

(local colours (require :ansicolors))
;; (local logos (require :logos))
;; (local rc (require :rc))


(local logos (require :config.logos))
(local rc (require :config.rc))


(local data rc.data)
(local logo rc.logo)
(local indent rc.indent)

(fn draw []
  ;;	(print (.. indent  (. header :key) (. header :value)))
  ;;	(print (.. indent  "-----------------"))
  (each [i v (ipairs logo)]
	(table.insert data {:key "" :value ""})
	(print
	 (colours.noReset
	  (table.concat
	   [(.. (. logo i) "%{reset}")
		(.. (. (. data i) :key) "%{reset}")
		(.. (. (. data i) :value) "%{reset}")])))))

(fn main []
  "Standard main function."
  (draw)
  )

(main)
;;{: rc}
