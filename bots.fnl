#!/usr/bin/env fennel
(local bots {})
(local projname "fnlfetch")
(local tgt (.. "~/fennelbin/" projname))
(local tgtlicense (.. "~/fennelbin/licenses/" projname ".org"))


(fn exec [t sep]
  "This is a simple wrapper around os.execute,
but wrapping the argument in <nil-safe> (table.concat t sep).
if sep? is nil, it will use an empty string, so remember to add spaces where needed"
  (print (table.concat t (or sep "")))
  (os.execute (table.concat t (or sep "")))
  )


(fn bots.install [self]
  (exec ["install " "--compare " (.. projname ".fnl") tgt] " ")
  (exec ["install " "--compare " "LICENSE.org" tgtlicense] " ")
  (print "done")
  )

(fn bots.config [self]
  (exec ["install " "--compare " (.. projname ".fnl") ] " ")
  (print "done")
  )

(fn bots.uninstall [self]
  (exec ["rm" "-f" tgt ] " ")
  (exec ["rm" "-f" tgtlicense] " ")
  (print "done")
  )

bots
