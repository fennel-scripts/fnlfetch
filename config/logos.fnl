(local logos {})


(tset logos :startrek [
 "                   .                        "
 "                  .:.                       "
 "                 .:::.                      "
 "                .:::::.                     "
 "            ***.:::::::.***                 "
 "       *******.:::::::::.*******            "
 "     ********.:::::::::::.********          "
 "    ********.:::::::::::::.********         "
 "    *******.::::::'***`::::.*******         "
 "    ******.::::'*********`::.******         "
 "     ****.:::'*************`:.****          "
 "       *.::'*****************`.*            "
 "       .:'  ***************    .            "
 "      .                                     "
]
)







(tset logos :startrekColour [
 "                   %{blue}.%{reset}                        "
 "                  %{blue}.:.%{reset}                       "
 "                 %{blue}.:::.%{reset}                      "
 "                %{blue}.:::::.%{reset}                     "
 "            ***%{blue}.:::::::.%{reset}***                 "
 "       *******%{blue}.:::::::::.%{reset}*******            "
 "     ********%{blue}.:::::::::::.%{reset}********          "
 "    ********%{blue}.:::::::::::::.%{reset}********         "
 "    *******%{blue}.::::::'%{reset}***%{blue}`::::.%{reset}*******         "
 "    ******%{blue}.::::'%{reset}*********%{blue}`::.%{reset}******         "
 "     ****%{blue}.:::'%{reset}*************%{blue}`:.%{reset}****          "
 "       *%{blue}.::'%{reset}*****************%{blue}`.%{reset}*            "
 "       %{blue}.:'%{reset}  ***************%{blue}    .%{reset}            "
 "      %{blue}.%{reset}                                     "
]
)

(tset logos :altStartrekColour [
 "                   %{blue}⌃%{reset}                        "
 "                  %{blue}/:\\%{reset}                       "
 "                 %{blue}/:::\\%{reset}                      "
 "                %{blue}/:::::\\%{reset}                     "
 "            ***%{blue}/:::::::\\%{reset}***                 "
 "       *******%{blue}/:::::::::\\%{reset}*******            "
 "     ********%{blue}/:::::::::::\\%{reset}********          "
 "    ********%{blue}/:::::::::::::\\%{reset}********         "
 "    *******%{blue}/::::::/%{reset}***%{blue}\\::::\\%{reset}*******         "
 "    ******%{blue}/::::/%{reset}*********%{blue}\\::\\%{reset}******         "
 "     ****%{blue}/:::/%{reset}*************%{blue}\\:\\%{reset}****          "
 "       *%{blue}/::/%{reset}*****************%{blue}\\\\%{reset}*            "
 "       %{blue}/:/%{reset}  ***************%{blue}    \\%{reset}            "
 "      %{blue}/%{reset}                                     "
]
)

(tset logos :startrekFancyColour
	  [
	   "                   %{blue}▒%{reset}                        "
	   "                  %{blue}▒▓▒%{reset}                       "
	   "                 %{blue}▒▓▓▓▒%{reset}                      "
	   "                %{blue}▒▓▓▓▓▓▒%{reset}                     "
	   "            ▓▓▓%{blue}▒▓▓▓▓▓▓▓▒%{reset}▓▓▓                 "
	   "       ▓▓▓▓▓▓▓%{blue}▒▓▓▓▓▓▓▓▓▓▒%{reset}▓▓▓▓▓▓▓            "
	   "     ▓▓▓▓▓▓▓▓%{blue}▒▓▓▓▓▓▓▓▓▓▓▓▒%{reset}▓▓▓▓▓▓▓▓          "
	   "    ▓▓▓▓▓▓▓▓%{blue}▒▓▓▓▓▓▓▓▓▓▓▓▓▓▒%{reset}▓▓▓▓▓▓▓▓         "
	   "    ▓▓▓▓▓▓▓%{blue}▒▓▓▓▓▓▓▒%{reset}▓▓▓%{blue}▒▓▓▓▓▒%{reset}▓▓▓▓▓▓▓         "
	   "    ▓▓▓▓▓▓%{blue}▒▓▓▓▓▒%{reset}▓▓▓▓▓▓▓▓▓%{blue}▒▓▓▒%{reset}▓▓▓▓▓▓         "
	   "     ▓▓▓▓%{blue}▒▓▓▓▒%{reset}▓▓▓▓▓▓▓▓▓▓▓▓▓%{blue}▒▓▒%{reset}▓▓▓▓          "
	   "       ▓%{blue}▒▓▓▒%{reset}▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓%{blue}▒▒%{reset}▓            "
	   "       %{blue}▒▓▒%{reset}  ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓%{blue}    ▒%{reset}            "
	   "      %{blue}▒%{reset}                                     "
	   ]
	  )




(local tls "_(_%{yellow}@%{reset}%{blue}_)_");;tardis light string

(local twn " |%{whitebg}≡≡≡%{reset}%{blue}| ");;tardis white note

(tset logos :tardis
[
(..            "                                  ")
(.. "%{blue} " "              ___                 ")
(.. "%{blue} " "      ______" tls "______         ")
(.. "%{blue} " "      | POLICE      BOX |         ")
(.. "%{blue} " "      |_________________|         ")
(.. "%{blue} " "       | _____ | _____ |          ")
(.. "%{blue} " "       | |###| | |###| |          ")
(.. "%{blue} " "       | |###| | |###| |          ")
(.. "%{blue} " "       | _____ | _____ |          ")
(.. "%{blue} " "       |" twn "| || || |          ")
(.. "%{blue} " "       |" twn "| ||_|| |          ")
(.. "%{blue} " "       | _____ |$_____ |          ")
(.. "%{blue} " "       | || || | || || |          ")
(.. "%{blue} " "       | ||_|| | ||_|| |          ")
(.. "%{blue} " "       | _____ | _____ |          ")
(.. "%{blue} " "       | || || | || || |          ")
(.. "%{blue} " "       | ||_|| | ||_|| |          ")
(.. "%{blue} " "       |       |       |          ")
(.. "%{blue} " "     [===================]         ")
;;(.. "%{blue} " "      *******************         ")
;;(.. "%{blue} " "       *****************          ")
]
)
(tset logos :tux ;;"borrowed" from neofetch
	  [
	   (.. "%{black}" "        #####              ")
	   (.. "%{black}" "       #######             ")
	   (.. "%{black}" "       ##%{white}O%{black}#%{white}O%{black}##                 ")
	   (.. "%{black}" "       #%{yellow}#####%{black}#                 ")
	   (.. "%{black}" "     ##%{white}##%{yellow}###%{white}##%{black}##               ")
	   (.. "%{black}" "    #%{white}##########%{black}##              ")
	   (.. "%{black}" "   #%{white}############%{black}##             ")
	   (.. "%{black}" "   #%{white}############%{black}###             ")
	   (.. "%{yellow}" "  ##%{black}#%{white}###########%{black}##%{yellow}#          ")
	   (.. "%{yellow}" "######%{black}#%{white}#######%{black}#%{yellow}######          ")
	   (.. "%{yellow}" "#######%{black}#%{white}#####%{black}#%{yellow}#######          ")
	   (.. "%{yellow}" "  #####%{black}#######%{yellow}#####          ")
	   ])




(tset logos :quake
[
"   _______________________________________   "
"  |\\ ___________________________________ /|  "
"  | | _                               _ | |  "
"  | |(+)        _           _        (+)| |  "
"  | | ~      _--/           \\--_      ~ | |  "
"  | |       /  /             \\  \\       | |  "
"  | |      /  |               |  \\      | |  "
"  | |     /   |               |   \\     | |  "
"  | |     |   |    _______    |   |     | |  "
"  | |     |   |    \\     /    |   |     | |  "
"  | |     \\    \\_   |   |   _/    /     | |  "
"  | |      \\     -__|   |__-     /      | |  "
"  | |       \\_                 _/       | |  "
"  | |         --__         __--         | |  "
"  | |             --|   |--             | |  "
"  | |               |   |               | |  "
"  | |                | |                | |  "
"  | |                 |                 | |  "
"  | |                                   | |  "
"  | |   I S   G O O D   F O R   Y O U   | |  "
"  | | _                               _ | |  "
"  | |(+)                             (+)| |  "
"  | | ~                               ~ | |  "
"  |/ ----------------------------------- \\|  "
"   ---------------------------------------   "
])

logos
