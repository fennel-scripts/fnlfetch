(local nova (require :supernova))

(local fennel  (require "fennel"))

(local confpath (os.getenv "XDG_CONFIG_DIR"))
(set package.path (.. package.path ";" confpath "/fnlFetch/?.fnl"))
(set fennel.path (.. fennel.path ";" confpath "/fnlFetch/?.fnl"))
(table.insert package.path (.. confpath "(fnlfetch/?.fnl"))

(local settings (require :systemSettings))

(local logos (require :logos))

(local pallet
	   {
		:one
		["%{blackbg}"
		 "%{redbg}"
		 "%{greenbg}"
		 "%{yellowbg}"
		 ]
		:two
		["%{bluebg}"
		 "%{magentabg}"
		 "%{cyanbg}"
		 "%{whitebg}"
		 "%{reset}"]})



(local data
	   {:default
		[
		 {:key "                 " :value " "}
		 {:key "                 " :value " "}
		 {:key "       erik%{reset}@" :value (.. "%{blue}" settings.hostname)   }
		 {:key "      [=========" :value "========]  "}
		 {:key "%{blue}OS               %{reset}" :value (.. ":" settings.distribution) }
		 {:key "%{blue}Home Directory   %{reset}" :value ":/home/erik"  }
		 {:key "%{blue}shell            %{reset}" :value (.. ":" settings.shell) }
		 {:key "%{blue}Window Manager   %{reset}" :value ":Awesome"     }
		 {:key "%{blue}Editor           %{reset}" :value ":Emacs"       }
		 {:key (table.concat (. pallet 1) "   ")  :value (table.concat (. pallet 2) "   ")}
		 {:key "                 " :value " "}
		 {:key "                 " :value " "}
		 {:key "                 " :value " "}]})

;;      {:key "%{blue}username         %{reset}" :value ":erik"            }


(local rc {:logo logos.tardis :data data.default})

rc
