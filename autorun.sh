#!/bin/env bash

#inotifywait -e modify -m test.fnl
inotifywait -q -m -e close_write main.fnl |
while read -r filename event; do
	tym $1 -e ./main.fnl         # or "./$filename"
done
