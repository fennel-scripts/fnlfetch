#!/usr/bin/env fennel
(local fennel  (require "fennel"))
;;(table.insert (or package.loaders package.searchers) fennel.searcher)

;;(local confpath (os.getenv "XDG_CONFIG_DIR"))
;;(set package.path (.. package.path ";" confpath "/fnlFetch/?.fnl"))
;;(set fennel.path (.. fennel.path ";" confpath "/fnlFetch/?.fnl"))
;;(table.insert package.path (.. confpath "(fnlfetch/?.fnl"))

(local colours (require :ansicolors))
;;(local logos (require :logos))
;;(local rc (require :rc))


(local logos (require :config.logos))
(local rc (require :config.rc))

(local data rc.data)
(local logo (. logos rc.logo))
;;(local indent rc.indent)
;;(print data "," logo "," indent)
;;(print (fennel.view data))




(fn draw []
;;	(print (.. indent  (. header :key) (. header :value)))
;;	(print (.. indent  "-----------------"))
	(each [i v (ipairs logo)]
	  (table.insert data {:key "" :value ""})
	  (local dat (. data i))
	  (local art (. logo i))
	  (print
	   (colours.noReset
		(table.concat
		 [art dat.key dat.value "%{reset}"])
		)
	   )
	  )
	)

(fn main [] ;;"Standard main function."
(draw)
;;(print (colours "hello world"))
)

(main)
